import pkg from './package'
import axios from 'axios'

export default {
  mode: 'universal',
  ssr: true,

  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#E53E3E' },

  /*
  ** Global CSS
  */
  css: ['@/assets/sass/pool.scss'],


  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/directives.js',
    '~/plugins/vue-currency-filter.js'
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    'nuxt-purgecss',
  ],
  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
  },

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
    },
    postcss: {
      plugins: {
        cssnano: {
          preset: 'default',
          discardComments: { removeAll: true },
          zIndex: false
        }
      },
      preset: {
        stage: 0,
        autoprefixer: {
          browsers: ['> 0%']
        }
      }
    },
    extractCSS: true

  },

  generate: {
    /*routes: function () {
      return axios.get('https://jsonplaceholder.typicode.com/posts')
        .then((res) => {
          return res.data.map((post) => {
            return {
              route: '/posts/' + post.id,
              payload: post
            }
          })
        })
    }*/
  },

  server: {
    host: '0.0.0.0',
  },

  purgeCSS: {
    enabled: false,
    extractors: [
      {
        extractor: class {
          static extract(content) {
            return content.match(/[A-Za-z0-9-_@:\/]+/g) || [];
          }
        },
        extensions: ['vue']
      }
    ]
  }
}
